import csv
import os

data = []

for name in os.listdir('data'):
    file = 'data/' + name
    print(file)
    with open(file, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:
            if row[0] != 'haloID':
                data.append(row)

print("number of rows:",len(data))

temp_haloID = data[0][0]
j = 0
signals = []

for i in range(len(data)):
    temp_row = []
    print(i)
    while data[i][0] == temp_haloID:
        if temp_row == []:
            temp_row = data[i][1:13]
            print(len(temp_row))
        elif len(temp_row) > 3 and len(temp_row) < 132:
            prog_data = data[i][15:27]
            temp_row = temp_row + prog_data
        if len(temp_row) > 131:
            i += 1
    #if len(temp_row) < 132:
    #    while len(temp_row) < 132:
    #        temp_row = temp_row = '0'
#signals.append(temp_row)
#    temp_haloID = data[i][0]
#    print(temp_row)