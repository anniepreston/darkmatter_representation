//file management
var prefix = "https://bitbucket.org/annie_preston/darkmatter_representation/raw/3999da89754db729faa655bdd3e1dd1b5b3bbfb6/";

var DL_signif_files = [prefix + "results/signif_DL_hp_3.csv", prefix + "results/signif_DL_hp_5.csv", prefix + "results/signif_DL_hp_10.csv"];
var DL_code_files = [prefix + "comparison_results/sparse_code_1_hp_3.csv", prefix + "comparison_results/sparse_code_1_hp_5.csv", prefix + "comparison_results/sparse_code_1_hp_10.csv"];
var DL_hyper_files = [prefix + "comparison_results/dictionary_1_hp_3.csv", prefix +  "comparison_results/dictionary_1_hp_5.csv", prefix + "comparison_results/dictionary_1_hp_10.csv"];
var DL_predictions_blackhole = [prefix + "comparison_results/predictions_1_DL_hp_3_blackhole.csv", prefix + "comparison_results/predictions_1_DL_hp_5_blackhole.csv", prefix + "comparison_results/predictions_1_DL_hp_10_blackhole.csv"];
var DL_predictions_bulge = [prefix + "comparison_results/predictions_1_DL_hp_3_bulge.csv", prefix + "comparison_results/predictions_1_DL_hp_5_bulge.csv", prefix + "comparison_results/predictions_1_DL_hp_10_bulge.csv"];
var DL_predictions_coldgas = [prefix + "comparison_results/predictions_1_DL_hp_3_coldgas.csv", prefix + "comparison_results/predictions_1_DL_hp_5_coldgas.csv", prefix + "comparison_results/predictions_1_DL_hp_10_coldgas.csv"];
var DL_predictions_stellar = [prefix + "comparison_results/predictions_1_DL_hp_3_stellar.csv", prefix + "comparison_results/predictions_1_DL_hp_5_stellar.csv", prefix + "comparison_results/predictions_1_DL_hp_10_stellar.csv"];
var DL_hyperparams = [3, 5, 10];

var AE_signif_files = [prefix + "results/signif_AE_hp_3.csv", prefix + "results/signif_AE_hp_5.csv", prefix + "results/signif_AE_hp_10.csv"];
var AE_code_files = [prefix + "comparison_results/autoencode_1_hp_3.csv", prefix + "comparison_results/autoencode_1_hp_5.csv", prefix + "comparison_results/autoencode_1_hp_10.csv"];
var AE_hyper_files = [prefix + "comparison_results/autoencoder_1_3.csv", prefix + "comparison_results/autoencoder_1_5.csv", prefix + "comparison_results/autoencoder_1_10.csv"];
var AE_predictions_blackhole = [prefix + "comparison_results/predictions_1_AE_hp_3_blackhole.csv", prefix + "comparison_results/predictions_1_AE_hp_5_blackhole.csv", prefix + "comparison_results/predictions_1_AE_hp_10_blackhole.csv"];
var AE_predictions_bulge = [prefix + "comparison_results/predictions_1_AE_hp_3_bulge.csv", prefix + "comparison_results/predictions_1_AE_hp_5_bulge.csv", prefix + "comparison_results/predictions_1_AE_hp_10_bulge.csv"];
var AE_predictions_coldgas = [prefix + "comparison_results/predictions_1_AE_hp_3_coldgas.csv", prefix + "comparison_results/predictions_1_AE_hp_5_coldgas.csv", prefix + "comparison_results/predictions_1_AE_hp_10_coldgas.csv"];
var AE_predictions_stellar = [prefix + "comparison_results/predictions_1_AE_hp_3_stellar.csv", prefix + "comparison_results/predictions_1_AE_hp_5_stellar.csv", prefix + "comparison_results/predictions_1_AE_hp_10_stellar.csv"];

var galaxy_file = [prefix + "comparison_results/normed_galaxies_1.csv"];
var raw_galaxy_file = [prefix + "comparison_results/raw_galaxies_1.csv"];
var DL_predictions = [DL_predictions_coldgas, DL_predictions_stellar, DL_predictions_bulge, DL_predictions_blackhole];
var AE_predictions = [AE_predictions_coldgas, AE_predictions_stellar, AE_predictions_bulge, AE_predictions_blackhole];
var hist_names = ['cold gas mass', 'stellar mass', 'bulge mass', 'black hole mass'];
var data_options = ['histogram', 'scatter'];
var type_options = ['sparse coding', 'autoencoder'];
var data_type = 'histogram';
var current_param = 'black hole mass';
var current_num = 1;
var current_method = 'sparse coding';

//set up layout
var margin = {top: 200, right: 20, bottom: 30, left: 50},
    width = 1400 - margin.left - margin.right,
    height = 1200 - margin.top - margin.bottom;
  
var svg = d3.select("body").append("svg")
	.attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");  

//button for selecting # of hyperparameters, data display options, and type of representation:
var allButtons = svg.append("g")
	.attr("id", "allButtons");
var defaultColor= "#EBF4FA";
var hoverColor= colorbrewer.Blues[8][3];
var pressedColor= colorbrewer.Blues[8][4];
var buttonGroups= allButtons.selectAll("g.hpbutton")
		.data(DL_hyperparams)
        .enter()
		.append("g")
		.attr("class","hpbutton")
		.style("cursor","pointer")
		.on("click",function(d,i) {
			updateButtonColors(d3.select(this), d3.select(this.parentNode))
			d3.select("#numberToggle").text(i+1)
			updateSignif(i);
			num_radar = 0;
			updateHyper(i);
			num_hists = 0;
			updatePredHists(i);
			current_num = i;
		})
		.on("mouseover", function() {
			if (d3.select(this).select("rect").attr("fill") != pressedColor) {
				d3.select(this)
					.select("rect")
					.attr("fill",hoverColor);
			}
		})
		.on("mouseout", function() {
			if (d3.select(this).select("rect").attr("fill") != pressedColor) {
				d3.select(this)
					.select("rect")
					.attr("fill",defaultColor);
			}
		});
var dataButtons = allButtons.selectAll("g.databutton")
	.data(data_options)
	.enter()
	.append("g")
	.attr("class","databutton")
	.style("cursor","pointer")	
	.on("click",function(d,i) {
		updateButtonColors(d3.select(this), d3.select(this.parentNode))
		d3.select("#numberToggle").text(i+1)
		data_type = data_options[i];
		if (data_type == 'scatter'){
			d3.selectAll('.hphist').remove();
			d3.selectAll('.histxaxis').remove();
			d3.selectAll('.histyaxis').remove();
			console.log(current_param);
			updateHPScatter(current_num, hist_names.indexOf(current_param));
		}
	})
	.on("mouseover", function() {
		if (d3.select(this).select("rect").attr("fill") != pressedColor) {
			d3.select(this)
				.select("rect")
				.attr("fill",hoverColor);
		}
	})
	.on("mouseout", function() {
		if (d3.select(this).select("rect").attr("fill") != pressedColor) {
			d3.select(this)
				.select("rect")
				.attr("fill",defaultColor);
		}
	});
	
var typeButtons = allButtons.selectAll("g.typebutton")
	.data(type_options)
	.enter()
	.append("g")
	.attr("class","typebutton")
	.style("cursor","pointer")	
	.on("click",function(d,i) {
		updateButtonColors(d3.select(this), d3.select(this.parentNode))
		d3.select("#numberToggle").text(i+1)
		current_method = type_options[i];
		updateSignif(current_num);
		updateHyper(current_num);
		updatePredHists(current_num);
		updateHPScatter(current_num, current_param);
	})
	.on("mouseover", function() {
		if (d3.select(this).select("rect").attr("fill") != pressedColor) {
			d3.select(this)
				.select("rect")
				.attr("fill",hoverColor);
		}
	})
	.on("mouseout", function() {
		if (d3.select(this).select("rect").attr("fill") != pressedColor) {
			d3.select(this)
				.select("rect")
				.attr("fill",defaultColor);
		}
	});
		
var bWidth= 25; //button width
var dWidth = 80; //data option button width
var tWidth = 100; //type option
var bHeight= 25; //button height
var bSpace= 0; //space between buttons
var x0= -30; //x offset
var y0= 180; //y offset

buttonGroups.append("rect")
	.attr("class","buttonRect")
	.attr("width",bWidth)
	.attr("height",bHeight)
	.attr("x",function(d,i) {return x0+(bWidth+bSpace)*i;})
	.attr("y",y0)
	.attr("rx",5) //rx and ry give the buttons rounded corners
	.attr("ry",5)
	.attr("fill",defaultColor)
	
dataButtons.append("rect")
	.attr("class","buttonRect")
	.attr("width",dWidth)
	.attr("height",bHeight)
	.attr("x",function(d,i) {return x0+100+(dWidth+bSpace)*i;})
	.attr("y",y0)
	.attr("rx",5) //rx and ry give the buttons rounded corners
	.attr("ry",5)
	.attr("fill",defaultColor)
	
typeButtons.append("rect")
	.attr("class","buttonRect")
	.attr("width",tWidth)
	.attr("height",bHeight)
	.attr("x",function(d,i) {return x0+285+(tWidth+bSpace)*i;})
	.attr("y",y0)
	.attr("rx",5) //rx and ry give the buttons rounded corners
	.attr("ry",5)
	.attr("fill",defaultColor)


//adding text to each toggle button group, centered 
//within the toggle button rect
buttonGroups.append("text")
	.attr("class","buttonText")
	.attr("font-family","sans-serif")
	.attr("x",function(d,i) {
		return x0 + (bWidth+bSpace)*i + bWidth/2;
	})
	.attr("y",y0+bHeight/2)
	.attr("text-anchor","middle")
	.attr("dominant-baseline","central")
	.attr("fill","steelBlue")
	.text(function(d) {return d;})
	
dataButtons.append("text")
	.attr("class","buttonText")
	.attr("font-family","sans-serif")
	.attr("x",function(d,i) {
		return x0 + 100 + (dWidth+bSpace)*i + dWidth/2;
	})
	.attr("y",y0+bHeight/2)
	.attr("text-anchor","middle")
	.attr("dominant-baseline","central")
	.attr("fill","steelBlue")
	.text(function(d) {return d;})

typeButtons.append("text")
	.attr("class","buttonText")
	.attr("font-family","sans-serif")
	.attr("x",function(d,i) {
		return x0 + 285 + (tWidth+bSpace)*i + tWidth/2;
	})
	.attr("y",y0+bHeight/2)
	.attr("text-anchor","middle")
	.attr("dominant-baseline","central")
	.attr("fill","steelBlue")
	.text(function(d) {return d;})

function updateButtonColors(button, parent) {
	parent.selectAll("rect")
		.attr("fill",defaultColor)

	button.select("rect")
		.attr("fill",pressedColor)
};

//function for moving items to front
d3.selection.prototype.moveToFront = function() {
  return this.each(function(){
    this.parentNode.appendChild(this);
  });
};

svg.append("text")
	.attr("x", -500)
	.attr("y",-20)
	.attr("font-size", 16)
  	.attr("font-family", 'sans-serif')
  	.attr("font-weight", "bold")
	.text("Hyperparameters")
	.attr("transform", function(d) {
	  			return "rotate(-90)"
	})
	.attr('fill', '#657383'); 

//labels & titles
svg.append("text")
	.attr("x", 500)
	.attr("class", "revealtext")
	.attr("y",560)
	.attr("font-size", 16)
  	.attr("font-family", 'sans-serif')
  	.attr("font-weight", "bold")
	.text("Hyperparameter Significance")
	.style("visibility", "hidden")
	.attr('fill', '#657383'); 

svg.append("text")
	.attr("x", -860)
	.attr("y",-20)
	.attr("font-size", 16)
  	.attr("font-family", 'sans-serif')
  	.attr("font-weight", "bold")
	.text("Input Data Significance")
	.attr("transform", function(d) {
	  			return "rotate(-90)"
	})
	.attr('fill', '#657383'); 	 
	
svg.append("text")
	.attr("x", 1210)
	.attr("y",245)
	.attr("font-size", 16)
  	.attr("font-family", 'sans-serif')
  	.attr("font-weight", "bold")
	.text("predictions")
	.attr('fill', '#657383'); 		

var colorIndex = 0;
var sel_colors = [];

for (var i = 0; i < 8; i++) { sel_colors.push(colorbrewer.Set2[8][i]); }
  
updateSignif(current_num)
  
function updateSignif(num) {
	//first, remove old stuff:
	d3.select(".signalpath").remove();
	d3.selectAll(".node").remove();
	d3.selectAll(".bar").remove();

	d3.text("attributes.csv", function(text) {
		attribs = d3.csv.parseRows(text);    
  
		//make "attribs" into array:
		axis_labels = [];
		for (var i = 0; i < 132; i++) { axis_labels[i] = "a"; } 
		var filename;
		if (current_method == 'sparse coding'){ filename = DL_signif_files[num]; }
		else { filename = AE_signif_files[num]; }

		d3.text(filename, function(text) {
		  signals = [];
		  raw_data = d3.csv.parseRows(text)[1];
		  for (var i = 0; i < raw_data.length; i++){
			signals.push(parseFloat(raw_data[i]));
		  }
  
		  var x = d3.scale.linear()
			.domain([0, signals.length])
			.range([0,1200]);

		  var x_bars = d3.scale.ordinal()
			  .rangeRoundBands([0, 1200], .1);
	
		  var y = d3.scale.linear()
			.domain([Math.min(...signals), Math.max(...signals)])
			.range([900, 650]);
	
		  var y_bars = d3.scale.linear()
			.range([900, 0])
			.domain([0, d3.max(signals, function(d) { return y(d); })]);

		  var x_ticks = d3.scale.ordinal()
			.domain(attribs[0])
			.rangeRoundBands([0, 1200], .1);
		  
		  var line = d3.svg.line()
			.x(function(d, i){
				return x(i);
			})
			.y(function(d) {
				return y(d);
			})
		
		  var xAxis = d3.svg.axis()
			.scale(x_ticks)
			.ticks(0);
						
		  svg.append("svg:g")
				.attr("class", "x axis")
				.attr("transform", "translate(0," + 900 + ")")
				.call(xAxis);
				
		  //var node = d3.selectAll('.tick');
		  //node.style("visibility", "hidden");
		  //node.attr("fill", '#657383');
		  //node.attr("font-family", "sans-serif");
		  	
		  svg.select(".x.axis")
				.selectAll("text")
				.attr("font-size", "15px")
				.style("text-anchor", "end")
				.attr("dx", "-.8em")
				.attr("dy", ".15em")
				.attr("transform", function(d) {
					return "rotate(-65)"
				})
				.style("visibility", "hidden");
				
		  var barWidth = width/signals.length;
						  
		  svg.selectAll(".bar")
			  .data(signals)
			.enter().append("rect")
			  .attr("class", "bar")
			  .attr("x", function(d, i) { return x(i) - 0.5*barWidth; })
			  .attr("width", barWidth)
			  .attr("y", function(d) { return y(d); })
			  .attr("height", function(d) { return 900 - y(d); })
			  .attr("selected", "false")
			  .each(function(d, i) {
				var sel = d3.select(this);
				var state = false;
				sel.moveToFront();
				sel.on('mouseover', function(d0, i0) {
					var node = svg.select('.x.axis').selectAll("text").filter(function(d, id) { return id % 132 == i; } );
					node.style("visibility", "visible");
				});
				sel.on('mouseout', function(d0, i0) {
					var node = svg.select('.x.axis').selectAll("text").filter(function(d, id) { return id % 132 == i; } );
					if (sel.attr("selected") == "false") {
						node.style("visibility", "hidden");
					}
				});
				sel.on('click', function(d0, i0) {
					var node = svg.select('.x.axis').selectAll("text").filter(function(d, id) { return id % 132 == i; } );
					node.style("visibility", "visible");
					sel.attr("selected", "true");
					sel.moveToFront();
					state = !state;
					if (state) {
						sel.style('fill', function(i0) { return sel_colors[colorIndex % sel_colors.length]; });
						colorIndex++;
					} else {
						sel.style('fill', '#EBF4FA');
						colorIndex--;
					}
					svg.selectAll(".radarStroke")
						.each(function() {
							var sel = d3.select(this);
						});
					svg.selectAll(".radarCircle")
						.filter(function(d, id) { return id % 132 == i; } )
						.each(function() {
							var sel = d3.select(this);      				
							sel.style('fill', function(i0) { return sel_colors[(colorIndex-1) % sel_colors.length]; });
							sel.style('r', '4');
							sel.style('fill-opacity', '10');
							sel.moveToFront();
							sel.style('stroke', 'black');
						});
				});
			});
 
		  svg.append("svg:path").attr("d", line(signals)).attr("class", "signalpath");
		});
	}); 
};

var attribs = [];
//create radar charts for hyperparameters:
updateHyper(current_num)
var num_radar = 0;
function updateHyper(num) {
	d3.selectAll(".radarStroke").remove();
	d3.selectAll(".radarCircle").remove();
	d3.selectAll(".radarChart").remove();
	d3.selectAll(".radarArea").remove();
	d3.selectAll(".gridCircle").remove();
	d3.text("attributes.csv", function(text) {
		attribs = d3.csv.parseRows(text);
	
		var filename;
		if (current_method == 'sparse coding'){ filename = DL_hyper_files[num]; }
		else { filename = AE_hyper_files[num]; }
		d3.text(filename, function(text) {
			raw_data = d3.csv.parseRows(text);
			for (var i = 0; i < raw_data.length; i++){
				var hyperparams  = [];
				var row = [];
				for (var j = 0; j < raw_data[0].length; j++){
					row.push(parseFloat(raw_data[i][j]) + 1.5);
				}
				hyperparams.push(row);
				if (i < 5) {
					radarChartOptions.margin.left = 230 * (i);
					radarChartOptions.margin.top = 210;
				}
				if (i > 4){ 
					radarChartOptions.margin.top = 430; 
					radarChartOptions.margin.left = 230 * (i-5);
				};
				radarChartOptions.color = colorbrewer.Blues[8][i%2 + 4]
				RadarChart(".radarChart", hyperparams, attribs, radarChartOptions, num_radar);
				num_radar++;
			}
		});

	});
};

var color = d3.scale.ordinal()
			.range(colorbrewer.Set1[7]);
				
var radarChartOptions = {
	w: 200,
	h: 200,
	margin: {top: 60, right: 20, bottom: 20, left: width/3},
	maxValue: 0.5,
	levels: 5,
	roundStrokes: true,
	color: color
	};
	
//now make the histograms!
//histogram("hist", hist_data, 1250, 100*i + 50, hist_names[i], i);
var num_hists = 0;
var num_hp_hists = 0;
function histogram(num, id, input_data, x_pos, y_pos){

	var x_pos_array = [100, 330, 560, 790, 1020, 330, 1020, 790, 100, 560];
	var y_pos_array = [-230, -230, -230, -230, -230, -75, -75, -75, -75, -75];
	
	var index;
	if (id == 'hphist') {
		index = num_hp_hists; 
		x_pos = x_pos_array[num_hp_hists] - 100;
		y_pos = y_pos_array[num_hp_hists];
	}
	else {index = num_hists;}

	var vert_offset = 220 + num_hists*140;

	var x = d3.scale.linear()
    		.domain([-2.0, 2.0])
    		.range([0, 100]);
    		
	var data = d3.layout.histogram()
    		.bins(x.ticks(30))
    		(input_data);
    		        	    		    		
	var y = d3.scale.linear()
    		.domain([0, d3.max(data, function(d) { return d.y; })])
    		.range([0, 100]);
    var yAxisScale = d3.scale.linear()
    		.domain([0, d3.max(data, function(d) { return d.y; })])
    		.range([100, 0]);

	var xAxis = d3.svg.axis()
    		.scale(x)
    		.orient("bottom")
    		.ticks(5);
    var yAxis = d3.svg.axis()
    		.scale(yAxisScale)
    		.orient("left")
    		.ticks(0);
	
	d3.select(id).select("svg").remove();
	
	var g = svg.append("g")
			.attr("transform", "translate(" + (margin.left) + "," + (vert_offset) + ")")
			.attr("class", id);

	//Wrapper for the grid & axes
	var bar = g.append("g").attr("class", "axisWrapper");
	
	bar.selectAll(".histbar")
	    .data(data)
	    .enter().append("rect")
		.attr("class", "histbar"+id)
   		.attr("transform", function(d) {return "translate(" + x(d.x) + "," + 0 + ")" + " rotate(180," + x_pos + "," + y_pos + ")"})
   		.attr("fill", "steelblue")
   		.attr("x", x_pos)
   		.attr("y", y_pos)
    	.attr("width", 0.1*x(data[0].dx) - 1)
    	.attr("height", function(d) { return y(d.y); });
    	
    svg.append("text")
    	.attr("class", "bartext")
    	.attr("x", x_pos + 90)
		.attr("y", y_pos + vert_offset + 20)
		.attr("font-size", 14)
  		.attr("font-family", "sans-serif")
  		.attr("text-anchor", "middle")
  		.attr("fill", '#657383')
		.text(hist_names[num_hists]);
		
	var sel = d3.selectAll('.histbarhist');
	sel.on('click', function(d,i){
	//hide signal plot stuff:
		sel.style("visibility", "visible");
		var signals = d3.selectAll('.bar');
		signals.style("visibility", "hidden");
		var path = d3.selectAll(".signalpath");
		path.style("visibility", "hidden");
		var axis = d3.selectAll(".x.axis");
		axis.style("visibility", "hidden");
		var tick = d3.selectAll(".tick");
		tick.style("visibility", "hidden");
	//move bottom radar plots down:
		var radarplots = d3.selectAll('.radarWrapper').filter(function(d, i){ return i > 9; } ); //FIXME: fix this indexing
		radarplots.transition().attr("transform", function(d) { return "translate(" + 0 + "," + 320 + ")"; });
		var radaraxis = d3.selectAll('.axisWrapper');
		radaraxis.attr("visibility", "hidden");
		var radarcircle = d3.selectAll('.radarCircle').filter(function(d, i){ return d > 4; } );
		radarcircle.attr("visibility", "hidden");
		
	//select prediction:
		var text = d3.selectAll('.bartext')
			.attr("font-size", 14)
			.attr("font-weight", "normal")
			.filter(function(d, id){return id == Math.floor(i/20); })
			.attr("font-size", 18)
			.attr("font-weight", "bold");
		
		//show histograms:
		//var hphist = d3.selectAll('.histbarhphist');
		//hphist.style("visibility", "visible");
		//var rvltxt = d3.selectAll('.revealtext');
		//rvltxt.style("visibility", "visible");
		function selectParam(i){
			num_hp_hists = 0;
			updateHPHists(num, Math.floor(i/20));
			current_param = hist_names[Math.floor(i/20)];
			console.log(hist_names[Math.floor(i/20)]);
			//updateHPScatter(num, Math.floor(i/20));
		}
		selectParam(i);
	});
	
	// x-axis
	if (id == 'hphist'){
		svg.append("g")
			  .attr("class", "histxaxis")
			  .attr("transform", "translate(" + (margin.left + x_pos) +  "," + (vert_offset + y_pos) + ")")
			  .call(xAxis)
			.append("text")
			  .attr("class", "histlabel")
			  .attr("x", x_pos + 80)
			  .attr("y", 32)
			  .style("text-anchor", "end")
			  .text("HP value");

		// y-axis
		svg.append("g")
			  .attr("class", "histyaxis")
			  .attr("transform", "translate(" + (margin.left + x_pos) + "," + (vert_offset + y_pos - 100) + ")")
			  .call(yAxis)
			.append("text")
			  .attr("class", "histlabel")
			  .attr("transform", "translate(-26,10),rotate(-90)")
			  .attr("y", 6)
			  .attr("dy", ".71em")
			  .style("text-anchor", "end")
			  .text("occurrence");
	}
	
	if (id == 'hphist') { num_hp_hists++; }
	else { num_hists += 1; }
}

//histograms for predictions:
//var hist_data = new Array(DL_predictions.length);

function updatePredHists(num){
	num_hists = 0;
	d3.selectAll(".histbarhist").remove();
	var filename;
	if (current_method == 'sparse coding'){ filename = DL_predictions; }
	else { filename = AE_predictions; }
	for (var i = 0; i < DL_predictions.length; i++){
		d3.text(filename[i][num], function(text) {  // i determines property; num determines HP number
			var raw_data = d3.csv.parseRows(text);  		  // predicted property value for each instance
			d3.text(galaxy_file, function(text) {
				var gal_data = d3.csv.parseRows(text);
				var hist_data = new Array(raw_data[0].length);
				for(var j = 0; j < raw_data[0].length; j++){
					hist_data[j] = (raw_data[0][j] - gal_data[j][num]);
				}
				histogram(num, "hist", hist_data, 1170, 150);
			});	
		});
	};
};
updatePredHists(current_num);

//histograms for hyperparameters:
function updateHPHists(num, parameter){            //number of HPs; property being predicted
	d3.selectAll(".histbarhphist").remove();
	for (var i = 0; i < DL_hyperparams[num]; i++){ //outer loop: sparse_code file
		drawHPHist(num, parameter, i);
	}
}
function drawHPHist(num, parameter, i){
	d3.text(DL_code_files[num], function(text){
		var raw_sparse_code = d3.csv.parseRows(text);
		var sparse_code = new Array;
		for (var l = 0; l < raw_sparse_code.length; l++){ sparse_code.push(raw_sparse_code[l][i]);}
		d3.text(DL_predictions[parameter][num], function(text) {  // i determines property; num determines HP number
			var raw_data = d3.csv.parseRows(text);  		  // predicted property value for each instance
			d3.text(galaxy_file, function(text) {
				var gal_data = d3.csv.parseRows(text);
				var accuracy_data = new Array(raw_data[0].length);
				for(var j = 0; j < raw_data[0].length; j++){
					accuracy_data[j] = (raw_data[0][j] - gal_data[j][parameter]);
				}
				var hist_data = new Array;
				for (var k = 0; k < raw_data[0].length; k++){
					if (Math.abs(accuracy_data[k]) < 0.02){hist_data.push(sparse_code[k]);}
				}
				console.log(hist_data);
				histogram(num, "hphist", hist_data, 0, 0);
			});
		});
	});
}	

function updateHPScatter(num, parameter){            //number of HPs; property being predicted
	//d3.selectAll(".histbarhphist").remove();
	for (var i = 0; i < DL_hyperparams[num]; i++){ //outer loop: sparse_code file
		drawHPScatter(num, parameter, i);
	}
}
function drawHPScatter(num, parameter, i){
 	var filename;
 	var pred_name;
 	if (current_method == 'sparse coding') { filename = DL_code_files; pred_name = DL_predictions; }
 	else { filename = AE_code_files; pred_name = AE_predictions; }
	d3.text(filename[num], function(text){
		var raw_sparse_code = d3.csv.parseRows(text);
		var sparse_code = new Array;
		for (var l = 0; l < raw_sparse_code.length; l++){ sparse_code.push(raw_sparse_code[l][i]);}
		d3.text(pred_name[parameter][current_num], function(text) {  // i determines property; num determines HP number
			var raw_data = d3.csv.parseRows(text);  		  // predicted property value for each instance
			d3.text(galaxy_file, function(text) {
				var scatter_data = [{
						"hp" : 0,
						"gal" : 0
					}];
				var gal_data = d3.csv.parseRows(text);
				var min_gal = 1000;
				var max_gal = -1000;
				var accuracy_data = new Array(raw_data[0].length);
				for(var j = 0; j < raw_data[0].length; j++){
					accuracy_data[j] = (raw_data[0][j] - gal_data[j][parameter]);
				}
				var min_hp = 1000;
				var max_hp = -1000;
				var hp_data = new Array;
				for (var k = 0; k < raw_data[0].length; k++){
					if (Math.abs(accuracy_data[k]) < 0.02){
						hp_data.push(sparse_code[k]);
						if (parseFloat(sparse_code[k]) > max_hp){ max_hp = sparse_code[k];}
						if (parseFloat(sparse_code[k]) < min_hp){ min_hp = sparse_code[k];}
					}
				}
				var raw_gal_data = new Array;
				d3.text(raw_galaxy_file, function(text) {
					var raw_gal_text = d3.csv.parseRows(text);
					for (var k = 0; k < raw_data[0].length; k++){
						if (Math.abs(accuracy_data[k]) < 0.02){
							raw_gal_data.push(raw_gal_text[k][parameter]);
							if (parseFloat(raw_gal_text[k][parameter]) > max_gal){ max_gal = raw_gal_text[k][parameter];}
							if (parseFloat(raw_gal_text[k][parameter]) < min_gal){ min_gal = raw_gal_text[k][parameter];}
							var d = {
								"hp" : sparse_code[k],
								"gal" : raw_gal_text[k][parameter]
							};
							scatter_data.push(d);
						}
					}
					scatter(num, "hpscatter", scatter_data, min_hp, max_hp, min_gal, max_gal);
				});
			});
		});
	});
}

var num_scatter = 0;
function scatter(num, id, input_data, x_min, x_max, y_min, y_max){

	var x_pos_array = [100, 330, 560, 790, 1020, 330, 1020, 790, 100, 560];
	var y_pos_array = [230, 230, 230, 230, 230, 400, 400, 400, 400, 400];

	var x_pos = x_pos_array[num_scatter] - 100;
	var y_pos = y_pos_array[num_scatter];
	console.log(x_pos);
	console.log(y_pos);

	//scaling
	var xScale = d3.scale.linear()
		.domain([x_min, x_max])
		.range([0, 100]);
		
	var xAxis = d3.svg.axis()
		.scale(xScale)
		.ticks(4)
		.orient("bottom");
	
	var yScale = d3.scale.linear()
		.domain([y_min, y_max])
		.range([100, 0]);
		
	var yAxis = d3.svg.axis()
		.scale(yScale)
		.ticks(4)
		.orient("left");
	
	var g = svg.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")")
		.attr("class", "scatter");
	
	var scatter = g.append("g").attr("class", "scatterWrapper");
	
	// draw dots
	scatter.selectAll(".scatterdot")
		.data(input_data)
	  .enter().append("circle")
		.attr("class", "scatterdothp")
		.attr("r", 1)
		.attr("cx", function(d) { return x_pos + xScale(d.hp); })
		.attr("cy", function(d) { return y_pos + yScale(d.gal); })
		.style("fill", "steelBlue");

	// x-axis
	svg.append("g")
		  .attr("class", "x axis")
		  .attr("transform", "translate(" + (x_pos + 60) + "," + (y_pos + 300) + ")")
		  .call(xAxis)
		.append("text")
		  .attr("class", "label")
		  .attr("x", x_pos + 80)
		  .attr("y", 32)
		  .style("text-anchor", "end")
		  .text("HP value")

	// y-axis
	svg.append("g")
		  .attr("class", "y axis")
		  .attr("transform", "translate(" + (x_pos + 60) + "," + (y_pos + 200) + ")")
		  .call(yAxis)
		.append("text")
		  .attr("class", "label")
		  .attr("transform", "translate(-40,10),rotate(-90)")
		  .attr("y", 6)
		  .attr("dy", ".71em")
		  .style("text-anchor", "end")
		  .text(current_param);
	num_scatter++;
}